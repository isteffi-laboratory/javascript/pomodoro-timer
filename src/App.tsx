import "./App.css";
import PomodoroTimer from "./components/PomodoroTimer";
import React from "react";

function App() {
  return (
    <div className="App">
      <PomodoroTimer />
    </div>
  );
}

export default App;
