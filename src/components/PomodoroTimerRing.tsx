import "./PomodoroTimerRing.css";
import React, { useState } from "react";

interface IPomodoroTimerRingProps {
  totalTime: number;
  elapsedTime: number;
}

const polarToCartesian = (
  centerX: number,
  centerY: number,
  radius: number,
  angleInDegrees: number
) => {
  const angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0;
  return {
    x: centerX + radius * Math.cos(angleInRadians),
    y: centerY + radius * Math.sin(angleInRadians),
  };
};

const describeArc = (
  centerX: number,
  centerY: number,
  radius: number,
  startAngle: number,
  endAngle: number
) => {
  const start = polarToCartesian(centerX, centerY, radius, endAngle);
  const end = polarToCartesian(centerX, centerY, radius, startAngle);
  const arcSweep = endAngle - startAngle <= 180 ? "0" : "1";
  return [
    "M",
    start.x,
    start.y,
    "A",
    radius,
    radius,
    0,
    arcSweep,
    0,
    end.x,
    end.y,
  ].join(" ");
};

const PomodoroTimerRing = ({
  totalTime,
  elapsedTime,
}: IPomodoroTimerRingProps) => {
  const angle = (elapsedTime / totalTime) * 360;

  return (
    <div className="PomodoroTimerRing">
      <svg width="518" height="518" viewBox="0 0 518 518">
        {angle < 360 ? (
          <path
            id="arc1"
            fill="none"
            stroke="#900A0A"
            strokeWidth="9"
            d={describeArc(259, 259, 254, 0, angle)}
          />
        ) : (
          <circle
            strokeWidth="9px"
            cx="259"
            cy="259"
            r="254"
            fill="transparent"
          />
        )}
      </svg>
    </div>
  );
};

export default PomodoroTimerRing;
