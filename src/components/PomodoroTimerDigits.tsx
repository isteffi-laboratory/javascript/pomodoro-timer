import "./PomodoroTimerDigits.css";
import React, { useEffect, useState } from "react";

interface IPomodoroTimerCounterProps {
  totalTime: number;
  elapsedTime: number;
  configure: boolean;
  onConfigure: (newValue: number) => void;
}

const SECONDS_PER_MINUTES = 60;

const renderDigits = (number: number) =>
  number.toLocaleString("de-DE", {
    minimumIntegerDigits: 2,
  });

const PomodoroTimerDigits = ({
  totalTime,
  elapsedTime,
  configure,
  onConfigure,
}: IPomodoroTimerCounterProps) => {
  const difference = totalTime - elapsedTime;
  const minutes = Math.floor(difference / SECONDS_PER_MINUTES);
  const seconds = difference % SECONDS_PER_MINUTES;

  const [configuredMinutes, setConfiguredMinutes] = useState(minutes);
  const [configuredSeconds, setConfiguredSeconds] = useState(seconds);

  const handleInput = (value: string, type: "SECONDS" | "MINUTES") => {
    let newValue = Number(value);
    newValue = isNaN(newValue) ? 0 : newValue;

    if (type === "MINUTES") {
      newValue = newValue > 99 ? 99 : newValue;
      setConfiguredMinutes(newValue);
    } else {
      newValue = newValue > 59 ? 59 : newValue;
      setConfiguredSeconds(newValue);
    }
  };

  useEffect(() => {
    onConfigure(configuredMinutes * 60 + configuredSeconds);
  }, [configuredMinutes, configuredSeconds]);

  return (
    <div className="PomodoroTimerDigits">
      <div className="minutes">
        {configure ? (
          <input
            type="text"
            value={renderDigits(configuredMinutes)}
            onChange={(event) => handleInput(event.target.value, "MINUTES")}
          />
        ) : (
          <input type="text" value={renderDigits(minutes)} disabled />
        )}
      </div>
      <div className="colon">:</div>
      <div className="seconds">
        {configure ? (
          <input
            type="text"
            value={renderDigits(configuredSeconds)}
            onChange={(event) => handleInput(event.target.value, "SECONDS")}
          />
        ) : (
          <input type="text" value={renderDigits(seconds)} disabled />
        )}
      </div>
    </div>
  );
};

export default PomodoroTimerDigits;
