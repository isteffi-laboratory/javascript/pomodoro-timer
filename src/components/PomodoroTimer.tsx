import gearSVG from "../assets/images/gear.svg";
import "./PomodoroTimer.css";
import PomodoroTimerDigits from "./PomodoroTimerDigits";
import PomodoroTimerRing from "./PomodoroTimerRing";
import React, { useEffect, useRef, useState } from "react";

const MILLISECONDS_IN_SECONDS = 1000;

const PomodoroTimer = () => {
  const [started, setStarted] = useState(false);
  const [ended, setEnded] = useState(false);
  const [configure, setConfigure] = useState(false);

  const [totalTime, setTotalTime] = useState(60);
  const [elapsedTime, setElapsedTime] = useState(0);

  const interval = useRef<NodeJS.Timer>();

  useEffect(() => {
    setStarted(false);
    setEnded(false);
    setElapsedTime(0);
  }, [totalTime]);

  useEffect(() => {
    if (elapsedTime >= totalTime) {
      setStarted(false);
      setEnded(true);
    }
  }, [elapsedTime]);

  useEffect(() => {
    if (started && !ended && !configure) {
      interval.current = setInterval(() => {
        setElapsedTime((passed) => passed + 1);
      }, MILLISECONDS_IN_SECONDS);
    } else {
      clearInterval(interval.current);
    }
  }, [started, ended, configure]);

  return (
    <div className="PomodoroTimer">
      <div className="PomodoroTimerWrapper">
        <PomodoroTimerRing totalTime={totalTime} elapsedTime={elapsedTime} />
        <PomodoroTimerDigits
          totalTime={totalTime}
          elapsedTime={elapsedTime}
          configure={configure}
          onConfigure={(newValue) => setTotalTime(newValue)}
        />

        <div className="PomodoroTimerControls">
          <button
            disabled={ended || configure || totalTime === 0}
            onClick={() => !ended && setStarted(!started)}
          >
            {started ? "stop" : "start"}
          </button>
          <button onClick={() => setConfigure(!configure)}>
            <img src={gearSVG} alt="Settings" />
          </button>
        </div>
      </div>
    </div>
  );
};

export default PomodoroTimer;
